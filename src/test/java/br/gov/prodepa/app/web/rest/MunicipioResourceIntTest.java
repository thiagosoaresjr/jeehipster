package br.gov.prodepa.app.web.rest;

import br.gov.prodepa.app.JHipsterProdepaApp;

import br.gov.prodepa.app.domain.Municipio;
import br.gov.prodepa.app.repository.MunicipioRepository;
import br.gov.prodepa.app.service.MunicipioService;
import br.gov.prodepa.app.service.dto.MunicipioDTO;
import br.gov.prodepa.app.service.mapper.MunicipioMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MunicipioResource REST controller.
 *
 * @see MunicipioResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JHipsterProdepaApp.class)
public class MunicipioResourceIntTest {

    private static final String DEFAULT_NOME = "AAAAA";
    private static final String UPDATED_NOME = "BBBBB";

    @Inject
    private MunicipioRepository municipioRepository;

    @Inject
    private MunicipioMapper municipioMapper;

    @Inject
    private MunicipioService municipioService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restMunicipioMockMvc;

    private Municipio municipio;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MunicipioResource municipioResource = new MunicipioResource();
        ReflectionTestUtils.setField(municipioResource, "municipioService", municipioService);
        this.restMunicipioMockMvc = MockMvcBuilders.standaloneSetup(municipioResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Municipio createEntity(EntityManager em) {
        Municipio municipio = new Municipio()
                .nome(DEFAULT_NOME);
        return municipio;
    }

    @Before
    public void initTest() {
        municipio = createEntity(em);
    }

    @Test
    @Transactional
    public void createMunicipio() throws Exception {
        int databaseSizeBeforeCreate = municipioRepository.findAll().size();

        // Create the Municipio
        MunicipioDTO municipioDTO = municipioMapper.municipioToMunicipioDTO(municipio);

        restMunicipioMockMvc.perform(post("/api/municipios")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(municipioDTO)))
                .andExpect(status().isCreated());

        // Validate the Municipio in the database
        List<Municipio> municipios = municipioRepository.findAll();
        assertThat(municipios).hasSize(databaseSizeBeforeCreate + 1);
        Municipio testMunicipio = municipios.get(municipios.size() - 1);
        assertThat(testMunicipio.getNome()).isEqualTo(DEFAULT_NOME);
    }

    @Test
    @Transactional
    public void checkNomeIsRequired() throws Exception {
        int databaseSizeBeforeTest = municipioRepository.findAll().size();
        // set the field null
        municipio.setNome(null);

        // Create the Municipio, which fails.
        MunicipioDTO municipioDTO = municipioMapper.municipioToMunicipioDTO(municipio);

        restMunicipioMockMvc.perform(post("/api/municipios")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(municipioDTO)))
                .andExpect(status().isBadRequest());

        List<Municipio> municipios = municipioRepository.findAll();
        assertThat(municipios).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMunicipios() throws Exception {
        // Initialize the database
        municipioRepository.saveAndFlush(municipio);

        // Get all the municipios
        restMunicipioMockMvc.perform(get("/api/municipios?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(municipio.getId().intValue())))
                .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())));
    }

    @Test
    @Transactional
    public void getMunicipio() throws Exception {
        // Initialize the database
        municipioRepository.saveAndFlush(municipio);

        // Get the municipio
        restMunicipioMockMvc.perform(get("/api/municipios/{id}", municipio.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(municipio.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMunicipio() throws Exception {
        // Get the municipio
        restMunicipioMockMvc.perform(get("/api/municipios/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMunicipio() throws Exception {
        // Initialize the database
        municipioRepository.saveAndFlush(municipio);
        int databaseSizeBeforeUpdate = municipioRepository.findAll().size();

        // Update the municipio
        Municipio updatedMunicipio = municipioRepository.findOne(municipio.getId());
        updatedMunicipio
                .nome(UPDATED_NOME);
        MunicipioDTO municipioDTO = municipioMapper.municipioToMunicipioDTO(updatedMunicipio);

        restMunicipioMockMvc.perform(put("/api/municipios")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(municipioDTO)))
                .andExpect(status().isOk());

        // Validate the Municipio in the database
        List<Municipio> municipios = municipioRepository.findAll();
        assertThat(municipios).hasSize(databaseSizeBeforeUpdate);
        Municipio testMunicipio = municipios.get(municipios.size() - 1);
        assertThat(testMunicipio.getNome()).isEqualTo(UPDATED_NOME);
    }

    @Test
    @Transactional
    public void deleteMunicipio() throws Exception {
        // Initialize the database
        municipioRepository.saveAndFlush(municipio);
        int databaseSizeBeforeDelete = municipioRepository.findAll().size();

        // Get the municipio
        restMunicipioMockMvc.perform(delete("/api/municipios/{id}", municipio.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Municipio> municipios = municipioRepository.findAll();
        assertThat(municipios).hasSize(databaseSizeBeforeDelete - 1);
    }
}
