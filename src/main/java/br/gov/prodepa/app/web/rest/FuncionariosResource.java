package br.gov.prodepa.app.web.rest;

import com.codahale.metrics.annotation.Timed;

import br.gov.prodepa.app.service.FuncionariosService;
import br.gov.prodepa.app.service.dto.FuncionariosDTO;
import br.gov.prodepa.app.util.resteasy.MyPageable;
import br.gov.prodepa.app.web.rest.util.HeaderUtil;
import br.gov.prodepa.app.web.rest.util.PaginationUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

//import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Funcionarios.
 */
@Path("/funcionarios")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class FuncionariosResource {

    private final Logger log = LoggerFactory.getLogger(FuncionariosResource.class);
        
    @Inject
    private FuncionariosService funcionariosService;

    /**
     * POST  /funcionarios : Create a new funcionarios.
     *
     * @param funcionariosDTO the funcionariosDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new funcionariosDTO, or with status 400 (Bad Request) if the funcionarios has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    //@RequestMapping(value = "/funcionarios", method = RequestMethod.POST,   produces = MediaType.APPLICATION_JSON_VALUE)
    //@Timed
    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createFuncionarios(@Valid FuncionariosDTO funcionariosDTO) throws URISyntaxException {
        log.debug("REST request to save Funcionarios : {}", funcionariosDTO);
        if (funcionariosDTO.getId() != null) {
            return HeaderUtil.createFailureAlert(Status.BAD_REQUEST,"funcionarios", "idexists", "A new funcionario cannot already have an ID");
        }
        FuncionariosDTO result = funcionariosService.save(funcionariosDTO);
        
        return HeaderUtil.createEntityCreationAlert("/api/funcionarios/" + result.getId(), "funcionarios", result.getId().toString());
    }

    /**
     * PUT  /funcionarios : Updates an existing funcionarios.
     *
     * @param funcionariosDTO the funcionariosDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated funcionariosDTO,
     * or with status 400 (Bad Request) if the funcionariosDTO is not valid,
     * or with status 500 (Internal Server Error) if the funcionariosDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    //@RequestMapping(value = "/funcionarios",  method = RequestMethod.PUT,        produces = MediaType.APPLICATION_JSON_VALUE)    @Timed
    @PUT
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateFuncionarios(@Valid FuncionariosDTO funcionariosDTO) throws URISyntaxException {
        log.debug("REST request to update Funcionarios : {}", funcionariosDTO);
        if (funcionariosDTO.getId() == null) {
            return createFuncionarios(funcionariosDTO);
        }
        FuncionariosDTO result = funcionariosService.save(funcionariosDTO);
        return HeaderUtil.createEntityUpdateAlert("funcionarios", result);
    }

    /**
     * GET  /funcionarios : get all the funcionarios.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of funcionarios in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    //@RequestMapping(value = "/funcionarios",        method = RequestMethod.GET,        produces = MediaType.APPLICATION_JSON_VALUE)    @Timed
    @GET
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllFuncionarios(@BeanParam MyPageable pageable) throws URISyntaxException {
        log.debug("REST request to get a page of Funcionarios");
        Page<FuncionariosDTO> page = funcionariosService.findAll(pageable);
        
        return PaginationUtil.generatePaginationHttpHeadersOK(page, "/api/funcionarios");
    }

    /**
     * GET  /funcionarios/:id : get the "id" funcionarios.
     *
     * @param id the id of the funcionariosDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the funcionariosDTO, or with status 404 (Not Found)
     */
    //@RequestMapping(value = "/funcionarios/{id}",        method = RequestMethod.GET,        produces = MediaType.APPLICATION_JSON_VALUE)    @Timed
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFuncionarios(@PathParam("id") Long id) {
        log.debug("REST request to get Funcionarios : {}", id);
        FuncionariosDTO funcionariosDTO = funcionariosService.findOne(id);
        
        return Optional.ofNullable(funcionariosDTO)
                .map(result -> Response.ok(result).build())
                .orElse(Response.status(Status.NOT_FOUND).build());
    }

    /**
     * DELETE  /funcionarios/:id : delete the "id" funcionarios.
     *
     * @param id the id of the funcionariosDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    //@RequestMapping(value = "/funcionarios/{id}",        method = RequestMethod.DELETE,        produces = MediaType.APPLICATION_JSON_VALUE)    @Timed
    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteFuncionarios(@PathParam("id") Long id) {
        log.debug("REST request to delete Funcionarios : {}", id);
        funcionariosService.delete(id);
        return HeaderUtil.createEntityDeletionAlert("funcionarios", id);
    }

    /**
     * SEARCH  /_search/funcionarios?query=:query : search for the funcionarios corresponding
     * to the query.
     *
     * @param query the query of the funcionarios search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    //@RequestMapping(value = "/_search/funcionarios", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)  @Timed
    @GET
    @Path("/search")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchFuncionarios(@QueryParam("query") String query, @BeanParam MyPageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Funcionarios for query {}", query);
        Page<FuncionariosDTO> page = funcionariosService.search(query, pageable);
        //HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/funcionarios");
        //return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
        return PaginationUtil.generateSearchPaginationHttpHeaders(Status.OK, query, page, "/api/_search/funcionarios");
    }


}
