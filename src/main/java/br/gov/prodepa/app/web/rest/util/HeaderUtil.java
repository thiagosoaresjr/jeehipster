package br.gov.prodepa.app.web.rest.util;

import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;

import br.gov.prodepa.app.domain.Model;

/**
 * Utility class for HTTP headers creation.
 */
public class HeaderUtil {

    private static final Logger log = LoggerFactory.getLogger(HeaderUtil.class);

    public static HttpHeaders createAlert(String message, String param) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-jHipsterProdepaApp-alert", message);
        headers.add("X-jHipsterProdepaApp-params", param);
        return headers;
    }

    public static HttpHeaders createEntityCreationAlert(String entityName, String param) {
        return createAlert("A new " + entityName + " is created with identifier " + param, param);
    }

    public static HttpHeaders createEntityUpdateAlert(String entityName, String param) {
        return createAlert("A " + entityName + " is updated with identifier " + param, param);
    }

    public static HttpHeaders createEntityDeletionAlert(String entityName, String param) {
        return createAlert("A " + entityName + " is deleted with identifier " + param, param);
    }

    public static HttpHeaders createFailureAlert(String entityName, String errorKey, String defaultMessage) {
        log.error("Entity creation failed, {}", defaultMessage);
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-jHipsterProdepaApp-error", defaultMessage);
        headers.add("X-jHipsterProdepaApp-params", entityName);
        return headers;
    }
    
    //NEW
    
    public static Response createFailureAlert(Status status, String entityName, String errorKey, String defaultMessage) {
        log.error("Entity creation failed, {}", defaultMessage);
        
        ResponseBuilder builder = Response.status(status);
        
        builder.header("X-jHipsterProdepaApp-error", defaultMessage);
        builder.header("X-jHipsterProdepaApp-params", entityName);
        return builder.build();
    }
    
    public static Response createEntityCreationAlert(String uri, String entityName, String param) {
    	
    	ResponseBuilder builder;
		try {
			builder = Response.created(new URI(uri));
			createAlert(builder, "A new " + entityName + " is created with identifier " + param, param);
			return builder.build();
		} catch (URISyntaxException e) {
			return Response.serverError().build();
		}
    }
    
    public static Response createEntityUpdateAlert(String entityName, Model model) {
    	if(model == null) {
    		return createAlert(Response.ok(), "A " + entityName + " is updated ", "").build();
    	} 
        return createAlert(Response.ok(), "A " + entityName + " is updated with identifier " + model.getId(), model.getId().toString()).entity(model).build();
    }
    
    public static Response createEntityDeletionAlert(String entityName, Model model) {
        return createAlert(Response.ok(), "A " + entityName + " is deleted with identifier " + model.getId(), model.getId().toString()).build();
    }
    
    public static Response createEntityDeletionAlert(String entityName, Long id) {
        return createAlert(Response.ok(), "A " + entityName + " is deleted with identifier " + id, id.toString()).build();
    }
    
    public static ResponseBuilder createAlert(ResponseBuilder builder, String message, String param) {
    	
		builder.header("X-jHipsterProdepaApp-alert", message);
		builder.header("X-jHipsterProdepaApp-params", param);
		
		return builder;
    }
    
    
}
