package br.gov.prodepa.app.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.codahale.metrics.annotation.Timed;

import br.gov.prodepa.app.service.EstadoService;
import br.gov.prodepa.app.service.dto.EstadoDTO;
import br.gov.prodepa.app.util.resteasy.MyPageable;
import br.gov.prodepa.app.web.rest.util.HeaderUtil;
import br.gov.prodepa.app.web.rest.util.PaginationUtil;

/**
 * REST controller for managing Estado.
 */
@Path("/estados")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class EstadoResource {

    private final Logger log = LoggerFactory.getLogger(EstadoResource.class);
        
    @Inject
    private EstadoService estadoService;

    /**
     * POST  /estados : Create a new estado.
     *
     * @param estadoDTO the estadoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new estadoDTO, or with status 400 (Bad Request) if the estado has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createEstado(@Valid EstadoDTO estadoDTO) throws URISyntaxException {
        log.debug("REST request to save Estado : {}", estadoDTO);
        if (estadoDTO.getId() != null) {
            return HeaderUtil.createFailureAlert(Status.BAD_REQUEST,"estado", "idexists", "A new estado cannot already have an ID");
        }
        EstadoDTO result = estadoService.save(estadoDTO);
        
        return HeaderUtil.createEntityCreationAlert("/api/estados/" + result.getId(), "estado", result.getId().toString());
    }

    /**
     * PUT  /estados : Updates an existing estado.
     *
     * @param estadoDTO the estadoDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated estadoDTO,
     * or with status 400 (Bad Request) if the estadoDTO is not valid,
     * or with status 500 (Internal Server Error) if the estadoDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PUT
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateEstado(@Valid EstadoDTO estadoDTO) throws URISyntaxException {
        log.debug("REST request to update Estado : {}", estadoDTO);
        
        if (estadoDTO.getId() == null) {
            return createEstado(estadoDTO);
        }
        EstadoDTO result = estadoService.save(estadoDTO);
        
        return HeaderUtil.createEntityUpdateAlert("estado", result);
    }

    /**
     * GET  /estados : get all the estados.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of estados in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GET
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllEstados(@BeanParam MyPageable pageable) throws URISyntaxException {
        log.debug("REST request to get a page of Estados");
        Page<EstadoDTO> page = estadoService.findAll(pageable);
        return PaginationUtil.generatePaginationHttpHeadersOK(page, "/api/estados");
    }

    /**
     * GET  /estados/:id : get the "id" estado.
     *
     * @param id the id of the estadoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the estadoDTO, or with status 404 (Not Found)
     */
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEstado(@PathParam("id") Long id) {
        log.debug("REST request to get Estado : {}", id);
        EstadoDTO estadoDTO = estadoService.findOne(new Long(id));
        return Optional.ofNullable(estadoDTO)
            .map(result -> Response.ok(result).build())
            .orElse(Response.status(Status.NOT_FOUND).build());
    }
    
    
    /**
     * DELETE  /estados/:id : delete the "id" estado.
     *
     * @param id the id of the estadoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteEstado(@PathParam("id") Long id) {
        log.debug("REST request to delete Estado : {}", id);
        estadoService.delete(id);
        return HeaderUtil.createEntityDeletionAlert("estado", id);
    }

}
