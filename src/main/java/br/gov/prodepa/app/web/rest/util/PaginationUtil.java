package br.gov.prodepa.app.web.rest.util;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URISyntaxException;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

/**
 * Utility class for handling pagination.
 *
 * <p>
 * Pagination uses the same principles as the
 * <a href="https://developer.github.com/v3/#pagination">Github API</a>, and
 * follow <a href="http://tools.ietf.org/html/rfc5988">RFC 5988 (Link
 * header)</a>.
 */
public class PaginationUtil {

	public static HttpHeaders generatePaginationHttpHeaders(Page<?> page, String baseUrl) throws URISyntaxException {

		HttpHeaders headers = new HttpHeaders();
		headers.add("X-Total-Count", "" + page.getTotalElements());
		String link = "";
		if ((page.getNumber() + 1) < page.getTotalPages()) {
			link = "<" + generateUri(baseUrl, page.getNumber() + 1, page.getSize()) + ">; rel=\"next\",";
		}
		// prev link
		if ((page.getNumber()) > 0) {
			link += "<" + generateUri(baseUrl, page.getNumber() - 1, page.getSize()) + ">; rel=\"prev\",";
		}
		// last and first link
		int lastPage = 0;
		if (page.getTotalPages() > 0) {
			lastPage = page.getTotalPages() - 1;
		}
		link += "<" + generateUri(baseUrl, lastPage, page.getSize()) + ">; rel=\"last\",";
		link += "<" + generateUri(baseUrl, 0, page.getSize()) + ">; rel=\"first\"";
		headers.add(HttpHeaders.LINK, link);
		return headers;
	}

	private static String generateUri(String baseUrl, int page, int size) throws URISyntaxException {
		return UriComponentsBuilder.fromUriString(baseUrl).queryParam("page", page).queryParam("size", size)
				.toUriString();
	}

	// NEW

	public static Response generatePaginationHttpHeadersOK(Page<?> page, String baseUrl) throws URISyntaxException {
		return generatePaginationHttpHeaders(Status.OK, page, baseUrl);	
	}
	
	public static Response generatePaginationHttpHeaders(Status status, Page<?> page, String baseUrl)
			throws URISyntaxException {

		ResponseBuilder builder = Response.status(status);

		builder.header("X-Total-Count", "" + page.getTotalElements());
		
		String link = "";
		if ((page.getNumber() + 1) < page.getTotalPages()) {
			link = "<" + generateUri(baseUrl, page.getNumber() + 1, page.getSize()) + ">; rel=\"next\",";
		}
		// prev link
		if ((page.getNumber()) > 0) {
			link += "<" + generateUri(baseUrl, page.getNumber() - 1, page.getSize()) + ">; rel=\"prev\",";
		}
		// last and first link
		int lastPage = 0;
		if (page.getTotalPages() > 0) {
			lastPage = page.getTotalPages() - 1;
		}
		link += "<" + generateUri(baseUrl, lastPage, page.getSize()) + ">; rel=\"last\",";
		link += "<" + generateUri(baseUrl, 0, page.getSize()) + ">; rel=\"first\"";
		
		builder.header(HttpHeaders.LINK, link);
		return builder.entity(page.getContent()).build();
	}
	
	public static Response generatePaginationHttpHeaders(Status status, Page<?> page, List<?> result, String baseUrl)
			throws URISyntaxException {

		ResponseBuilder builder = Response.status(status);

		builder.header("X-Total-Count", "" + page.getTotalElements());
		
		String link = "";
		if ((page.getNumber() + 1) < page.getTotalPages()) {
			link = "<" + generateUri(baseUrl, page.getNumber() + 1, page.getSize()) + ">; rel=\"next\",";
		}
		// prev link
		if ((page.getNumber()) > 0) {
			link += "<" + generateUri(baseUrl, page.getNumber() - 1, page.getSize()) + ">; rel=\"prev\",";
		}
		// last and first link
		int lastPage = 0;
		if (page.getTotalPages() > 0) {
			lastPage = page.getTotalPages() - 1;
		}
		link += "<" + generateUri(baseUrl, lastPage, page.getSize()) + ">; rel=\"last\",";
		link += "<" + generateUri(baseUrl, 0, page.getSize()) + ">; rel=\"first\"";
		
		builder.header(HttpHeaders.LINK, link);
		return builder.entity(result).build();
	}
	
	public static Response generateSearchPaginationHttpHeaders(Status status, String query, Page<?> page, String baseUrl) throws URISyntaxException {

			ResponseBuilder builder = Response.status(status);
		
		
			builder.header("X-Total-Count", "" + page.getTotalElements());
			
	        String link = "";
	        if ((page.getNumber() + 1) < page.getTotalPages()) {
	            link = "<" + generateUri(baseUrl, page.getNumber() + 1, page.getSize()) + "&query=" + query + ">; rel=\"next\",";
	        }
	        // prev link
	        if ((page.getNumber()) > 0) {
	            link += "<" + generateUri(baseUrl, page.getNumber() - 1, page.getSize()) + "&query=" + query + ">; rel=\"prev\",";
	        }
	        // last and first link
	        int lastPage = 0;
	        if (page.getTotalPages() > 0) {
	            lastPage = page.getTotalPages() - 1;
	        }
	        link += "<" + generateUri(baseUrl, lastPage, page.getSize()) + "&query=" + query + ">; rel=\"last\",";
	        link += "<" + generateUri(baseUrl, 0, page.getSize()) + "&query=" + query + ">; rel=\"first\"";
	        builder.header(HttpHeaders.LINK, link);
	        return builder.entity(page.getContent()).build();
	    }
}
