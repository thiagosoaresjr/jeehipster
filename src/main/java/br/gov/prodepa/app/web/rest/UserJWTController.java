package br.gov.prodepa.app.web.rest;

import br.gov.prodepa.app.security.jwt.JWTConfigurer;
import br.gov.prodepa.app.security.jwt.TokenProvider;
import br.gov.prodepa.app.util.Spring;
import br.gov.prodepa.app.web.rest.vm.LoginVM;

import java.util.Collections;

import com.codahale.metrics.annotation.Timed;

import javax.inject.Inject;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

//@RestController
@Path("/authenticate")
public class UserJWTController {

	@Spring
    @Inject
    private TokenProvider tokenProvider;

    //@Autowired
    @Inject
    private AuthenticationManager authenticationManager;
    
    //@Inject
    @Context
    private HttpServletResponse httpResponse;
    
    //@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    //@Timed
    
    @POST
    @Path("/")
    public Response authorize(@Valid LoginVM loginVM) {

    	System.out.println("POST /authenticate/");
    	//TODO Pegar aki o Usuario e senha do CA a partir do token do GD
        UsernamePasswordAuthenticationToken authenticationToken =
            new UsernamePasswordAuthenticationToken(loginVM.getUsername(), loginVM.getPassword());

        try {
            Authentication authentication = this.authenticationManager.authenticate(authenticationToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            boolean rememberMe = (loginVM.isRememberMe() == null) ? false : loginVM.isRememberMe();
            
            String jwt = tokenProvider.createToken(authentication, rememberMe);
            
            
            httpResponse.addHeader(JWTConfigurer.AUTHORIZATION_HEADER, "Bearer " + jwt);
            
            
            return Response.ok(new JWTToken(jwt)).build();
        } catch (AuthenticationException exception) {
            return Response.status(Status.UNAUTHORIZED).entity(Collections.singletonMap("AuthenticationException",exception.getLocalizedMessage())).build();
        }
        
    }
    
    /**
     * Metodo novo para processar o login do GD
     * 
     * @param ksessionid
     * @return
     */
    @GET 
    @Path("/")
    public Response authorizeGovernoDigital(@CookieParam("KSESSIONID") String ksessionid) {
    	
    	System.out.println("GEt /authenticate/");
    	System.out.println(ksessionid);
    	
    	LoginVM loginVM = new LoginVM();
    	loginVM.setUsername("admin");
    	loginVM.setPassword("admin");
    	loginVM.setRememberMe(true);
        
		UsernamePasswordAuthenticationToken authenticationToken =
            new UsernamePasswordAuthenticationToken(loginVM .getUsername(), loginVM.getPassword());

        try {
            Authentication authentication = this.authenticationManager.authenticate(authenticationToken);
            
            System.out.println(authentication.getName());
            
            System.out.println(authentication.getCredentials());
            
            System.out.println(authentication.getPrincipal());
            
            
            SecurityContextHolder.getContext().setAuthentication(authentication);
            boolean rememberMe = (loginVM.isRememberMe() == null) ? false : loginVM.isRememberMe();
            
            String jwt = tokenProvider.createToken(authentication, rememberMe);
            
            
            httpResponse.addHeader(JWTConfigurer.AUTHORIZATION_HEADER, "Bearer " + jwt);
            
            
            return Response.ok(new JWTToken(jwt)).build();
    	

        } catch (AuthenticationException exception) {
            return Response.status(Status.UNAUTHORIZED).entity(Collections.singletonMap("AuthenticationException",exception.getLocalizedMessage())).build();
        }
    	
		//return Response.ok("OK").build();
    }
}
