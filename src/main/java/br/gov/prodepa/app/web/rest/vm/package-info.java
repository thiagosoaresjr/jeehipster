/**
 * View Models used by Spring MVC REST controllers.
 */
package br.gov.prodepa.app.web.rest.vm;
