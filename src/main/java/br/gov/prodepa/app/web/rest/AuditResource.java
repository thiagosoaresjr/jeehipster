package br.gov.prodepa.app.web.rest;

import br.gov.prodepa.app.service.AuditEventService;

import java.time.LocalDate;
import br.gov.prodepa.app.web.rest.util.PaginationUtil;
/*import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;*/

import java.net.URISyntaxException;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.websocket.server.PathParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * REST controller for getting the audit events.
 */
//@RestController
//@RequestMapping(value = "/management/jhipster/audits", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestScoped
@Path("/management/jhipster/audits")
@Produces(MediaType.APPLICATION_JSON)
public class AuditResource {

    private AuditEventService auditEventService;

    //@Inject
    /*public AuditResource(AuditEventService auditEventService) {
        this.auditEventService = auditEventService;
    }*/

    /**
     * GET  /audits : get a page of AuditEvents.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of AuditEvents in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    //@RequestMapping(method = RequestMethod.GET)
    @GET
    public Response getAll(Pageable pageable) throws URISyntaxException {
        /*Page<AuditEvent> page = auditEventService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/audits");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);*/
    	
    	
    	Page<AuditEvent> page = auditEventService.findAll(pageable);
        //HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/audits");
        //return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
        
        return PaginationUtil.generatePaginationHttpHeadersOK(page, "/api/audits");
    }

    /**
     * GET  /audits : get a page of AuditEvents between the fromDate and toDate.
     *
     * @param fromDate the start of the time period of AuditEvents to get
     * @param toDate the end of the time period of AuditEvents to get
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of AuditEvents in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */

    //@RequestMapping(method = RequestMethod.GET,  params = {"fromDate", "toDate"})
    @GET
    @Path("/{fromDate}/{toDate}")
    //public Response getByDates(@RequestParam(value = "fromDate") LocalDate fromDate, @RequestParam(value = "toDate") LocalDate toDate, Pageable pageable) throws URISyntaxException {
    public Response getByDates(@PathParam(value = "fromDate") LocalDate fromDate, @PathParam(value = "toDate") LocalDate toDate, Pageable pageable) throws URISyntaxException {

        /*Page<AuditEvent> page = auditEventService.findByDates(fromDate.atTime(0, 0), toDate.atTime(23, 59), pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/audits");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);*/
    	
    	Page<AuditEvent> page = auditEventService.findByDates(fromDate.atTime(0, 0), toDate.atTime(23, 59), pageable);
        return PaginationUtil.generatePaginationHttpHeadersOK(page, "/api/audits");
    }

    /**
     * GET  /audits/:id : get an AuditEvent by id.
     *
     * @param id the id of the entity to get
     * @return the ResponseEntity with status 200 (OK) and the AuditEvent in body, or status 404 (Not Found)
     */
    //@RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") Long id) {
        /*return auditEventService.find(id)
                .map((entity) -> new ResponseEntity<>(entity, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));*/
    	
    	return auditEventService.find(id)
                .map((entity) -> Response.ok().build())
                .orElse(Response.status(Status.NOT_FOUND).build());
    }
}
