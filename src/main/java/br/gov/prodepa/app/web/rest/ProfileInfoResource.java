package br.gov.prodepa.app.web.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.core.env.Environment;

import br.gov.prodepa.app.config.DefaultProfileUtil;
import br.gov.prodepa.app.config.JHipsterProperties;
import br.gov.prodepa.app.util.Spring;

@Path("/profile-info")
public class ProfileInfoResource {

	@Inject
    Environment env;

	@Spring
    @Inject
    private JHipsterProperties jHipsterProperties;

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public ProfileInfoResponse getActiveProfiles() {
    	System.out.println("GET /profile-info/");
        return new ProfileInfoResponse(DefaultProfileUtil.getActiveProfiles(env), getRibbonEnv());
    }

    private String getRibbonEnv() {
        String[] activeProfiles = DefaultProfileUtil.getActiveProfiles(env);
        String[] displayOnActiveProfiles = jHipsterProperties.getRibbon().getDisplayOnActiveProfiles();

        if (displayOnActiveProfiles == null) {
            return null;
        }

        List<String> ribbonProfiles = new ArrayList<>(Arrays.asList(displayOnActiveProfiles));
        List<String> springBootProfiles = Arrays.asList(activeProfiles);
        ribbonProfiles.retainAll(springBootProfiles);

        if (ribbonProfiles.size() > 0) {
            return ribbonProfiles.get(0);
        }
        return null;
    }

    class ProfileInfoResponse {

        public String[] activeProfiles;
        public String ribbonEnv;

        ProfileInfoResponse(String[] activeProfiles, String ribbonEnv) {
            this.activeProfiles = activeProfiles;
            this.ribbonEnv = ribbonEnv;
        }
    }
}
