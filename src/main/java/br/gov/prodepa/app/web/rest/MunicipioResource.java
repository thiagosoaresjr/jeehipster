package br.gov.prodepa.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.gov.prodepa.app.service.MunicipioService;
import br.gov.prodepa.app.web.rest.util.HeaderUtil;
import br.gov.prodepa.app.web.rest.util.PaginationUtil;
import br.gov.prodepa.app.service.dto.MunicipioDTO;
import br.gov.prodepa.app.util.resteasy.MyPageable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing Municipio.
 */
@Path("/municipios")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MunicipioResource {

    private final Logger log = LoggerFactory.getLogger(MunicipioResource.class);
        
    @Inject
    private MunicipioService municipioService;

    /**
     * POST  /municipios : Create a new municipio.
     *
     * @param municipioDTO the municipioDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new municipioDTO, or with status 400 (Bad Request) if the municipio has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @POST
    @Path("/")
    public Response createMunicipio(@Valid MunicipioDTO municipioDTO) throws URISyntaxException {
        log.debug("REST request to save Municipio : {}", municipioDTO);
        if (municipioDTO.getId() != null) {
            return HeaderUtil.createFailureAlert(Status.BAD_REQUEST, "municipio", "idexists", "A new municipio cannot already have an ID");
        }
        MunicipioDTO result = municipioService.save(municipioDTO);
        return HeaderUtil.createEntityCreationAlert("/api/municipios/" + result.getId(), "municipio", result.getId().toString());
    }

    /**
     * PUT  /municipios : Updates an existing municipio.
     *
     * @param municipioDTO the municipioDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated municipioDTO,
     * or with status 400 (Bad Request) if the municipioDTO is not valid,
     * or with status 500 (Internal Server Error) if the municipioDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PUT
    @Path("/")
    public Response updateMunicipio(@Valid MunicipioDTO municipioDTO) throws URISyntaxException {
        log.debug("REST request to update Municipio : {}", municipioDTO);
        if (municipioDTO.getId() == null) {
            return createMunicipio(municipioDTO);
        }
        MunicipioDTO result = municipioService.save(municipioDTO);
        return HeaderUtil.createEntityUpdateAlert("municipio", result);
    }

    /**
     * GET  /municipios : get all the municipios.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of municipios in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GET
    @Path("/")
    public Response getAllMunicipios(@BeanParam MyPageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Municipios");
        Page<MunicipioDTO> page = municipioService.findAll(pageable);
        return PaginationUtil.generatePaginationHttpHeadersOK(page, "/api/municipios");
    }

    /**
     * GET  /municipios/:id : get the "id" municipio.
     *
     * @param id the id of the municipioDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the municipioDTO, or with status 404 (Not Found)
     */
    @GET
    @Path("/{id}")
    public Response getMunicipio(@PathParam("id") Long id) {
        log.debug("REST request to get Municipio : {}", id);
        MunicipioDTO municipioDTO = municipioService.findOne(id);
        return Optional.ofNullable(municipioDTO)
            .map(result -> Response.ok(result).build())
            .orElse(Response.status(Status.NOT_FOUND).build());
    }

    /**
     * DELETE  /municipios/:id : delete the "id" municipio.
     *
     * @param id the id of the municipioDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DELETE
    @Path("/{id}")
    public Response deleteMunicipio(@PathParam("id") Long id) {
        log.debug("REST request to delete Municipio : {}", id);
        municipioService.delete(id);
        return HeaderUtil.createEntityDeletionAlert("municipio", id);
    }

}
