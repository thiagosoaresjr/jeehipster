package br.gov.prodepa.app.web.rest;

import com.codahale.metrics.annotation.Timed;

import br.gov.prodepa.app.domain.User;
import br.gov.prodepa.app.repository.UserRepository;
import br.gov.prodepa.app.security.SecurityUtils;
import br.gov.prodepa.app.service.MailService;
import br.gov.prodepa.app.service.UserService;
import br.gov.prodepa.app.service.dto.UserDTO;
import br.gov.prodepa.app.web.rest.vm.KeyAndPasswordVM;
import br.gov.prodepa.app.web.rest.vm.ManagedUserVM;
import br.gov.prodepa.app.web.rest.util.HeaderUtil;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestParam;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.*;

/**
 * REST controller for managing the current user's account.
 */
@Path("/")
public class AccountResource {

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    //@Inject
    private UserRepository userRepository;

    @Inject
    private UserService userService;

    //@Inject
    private MailService mailService;

    /**
     * POST  /register : register the user.
     *
     * @param managedUserVM the managed user View Model
     * @param request the HTTP request
     * @return the ResponseEntity with status 201 (Created) if the user is registered or 400 (Bad Request) if the login or e-mail is already in use
     */
    @POST
    @Path("/register")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response registerAccount(@Valid ManagedUserVM managedUserVM, HttpServletRequest request) {
    	System.out.println("POST /register");
    	return userRepository.findOneByLogin(managedUserVM.getLogin().toLowerCase())
                .map(user -> Response.status(Status.BAD_REQUEST).entity("login already in use").build())
                .orElseGet(() -> userRepository.findOneByEmail(managedUserVM.getEmail())
                    
                	.map(user -> Response.status(Status.BAD_REQUEST).entity("e-mail address already in use").build())
                    .orElseGet(() -> {
                        User user = userService.createUser(managedUserVM.getLogin(), managedUserVM.getPassword(),
                        managedUserVM.getFirstName(), managedUserVM.getLastName(), managedUserVM.getEmail().toLowerCase(),
                        managedUserVM.getLangKey());
                        String baseUrl = request.getScheme() + // "http"
                        "://" +                                // "://"
                        request.getServerName() +              // "myhost"
                        ":" +                                  // ":"
                        request.getServerPort() +              // "80"
                        request.getContextPath();              // "/myContextPath" or "" if deployed in root context

                        mailService.sendActivationEmail(user, baseUrl);
                        try {
							return Response.created(new URI("")).build();
						} catch (URISyntaxException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							return Response.status(Status.BAD_REQUEST).entity("Account create fail.").build();
						}
                    })
            );
    	
    }

    /**
     * GET  /activate : activate the registered user.
     *
     * @param key the activation key
     * @return the ResponseEntity with status 200 (OK) and the activated user in body, or status 500 (Internal Server Error) if the user couldn't be activated
     */
    @GET
    @Path("/activate")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response activateAccount(@RequestParam(value = "key") String key) {
    	System.out.println("GET /active");
        return userService.activateRegistration(key)
            .map(user -> Response.ok().build())
            .orElse(Response.serverError().build());
    }

    /**
     * GET  /authenticate : check if the user is authenticated, and return its login.
     *
     * @param request the HTTP request
     * @return the login if the user is authenticated
     */
    @GET
    @Path("/authenticate")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String isAuthenticated(HttpServletRequest request) {
        log.debug("REST request to check if the current user is authenticated");
        System.out.println("GET /authenticate");
        return request.getRemoteUser();
    }
    
    

    /**
     * GET  /account : get the current user.
     *
     * @return the ResponseEntity with status 200 (OK) and the current user in body, or status 500 (Internal Server Error) if the user couldn't be returned
     */
    @GET
    @Path("/account")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAccount() {
    	System.out.println("GET  /account ");
    	return Optional.ofNullable(userService.getUserWithAuthorities())
                .map(user -> Response.ok(new UserDTO(user)).build())
                .orElse(Response.serverError().build());
    }

    /**
     * POST  /account : update the current user information.
     *
     * @param userDTO the current user information
     * @return the ResponseEntity with status 200 (OK), or status 400 (Bad Request) or 500 (Internal Server Error) if the user couldn't be updated
     */
    @POST
    @Path("/account")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveAccount(@Valid UserDTO userDTO) {
    	
    	System.out.println("POST /account");
    	
    	Optional<User> existingUser = userRepository.findOneByEmail(userDTO.getEmail());
    	
        if (existingUser.isPresent() && (!existingUser.get().getLogin().equalsIgnoreCase(userDTO.getLogin()))) {
        	return HeaderUtil.createFailureAlert(Status.BAD_REQUEST, "user-management", "emailexists", "Email already in use");
        }
        return userRepository
            .findOneByLogin(SecurityUtils.getCurrentUserLogin())
            .map(u -> {
                userService.updateUser(userDTO.getFirstName(), userDTO.getLastName(), userDTO.getEmail(),
                    userDTO.getLangKey());
                return Response.ok().build();
            })
            .orElseGet(() -> Response.serverError().build());
    }

    /**
     * POST  /account/change_password : changes the current user's password
     *
     * @param password the new password
     * @return the ResponseEntity with status 200 (OK), or status 400 (Bad Request) if the new password is not strong enough
     */
    @POST
    @Path("/account/change_password")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public Response changePassword(String password) {
    	System.out.println("POST /account/change");
    	if (!checkPasswordLength(password)) {
            return Response.status(Status.BAD_REQUEST).entity("Incorrect password").build();
        }
        userService.changePassword(password);
        return Response.ok().build();
    }

    /**
     * POST   /account/reset_password/init : Send an e-mail to reset the password of the user
     *
     * @param mail the mail of the user
     * @param request the HTTP request
     * @return the ResponseEntity with status 200 (OK) if the e-mail was sent, or status 400 (Bad Request) if the e-mail address is not registered
     */
    @POST
    @Path("/account/reset_password/init")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public Response requestPasswordReset(String mail, HttpServletRequest request) {
    	System.out.println("POST /account/reset/init");
    	return userService.requestPasswordReset(mail)
                .map(user -> {
                    String baseUrl = request.getScheme() +
                        "://" +
                        request.getServerName() +
                        ":" +
                        request.getServerPort() +
                        request.getContextPath();
                    mailService.sendPasswordResetMail(user, baseUrl);
                    return  Response.ok().entity("e-mail was sent").build();
                }).orElse(Response.status(Status.BAD_REQUEST).entity("e-mail address not registered").build());
    	
    }

    /**
     * POST   /account/reset_password/finish : Finish to reset the password of the user
     *
     * @param keyAndPassword the generated key and the new password
     * @return the ResponseEntity with status 200 (OK) if the password has been reset,
     * or status 400 (Bad Request) or 500 (Internal Server Error) if the password could not be reset
     */
    @POST
    @Path("/account/reset_password/finish")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public Response finishPasswordReset(KeyAndPasswordVM keyAndPassword) {
    	
    	System.out.println("POST /coount/reset/finish");
    	
    	if (!checkPasswordLength(keyAndPassword.getNewPassword())) {
            return Response.status(Status.BAD_REQUEST).entity("Incorrect password").build();
        }
        return userService.completePasswordReset(keyAndPassword.getNewPassword(), keyAndPassword.getKey())
              .map(user -> Response.ok().build())
              .orElse(Response.serverError().build());
    }

    private boolean checkPasswordLength(String password) {
        return (!StringUtils.isEmpty(password) &&
            password.length() >= ManagedUserVM.PASSWORD_MIN_LENGTH &&
            password.length() <= ManagedUserVM.PASSWORD_MAX_LENGTH);
    }
}
