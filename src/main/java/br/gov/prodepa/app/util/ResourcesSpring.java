package br.gov.prodepa.app.util;

import javax.enterprise.inject.Produces;

import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;

import br.gov.prodepa.app.config.JHipsterProperties;
import br.gov.prodepa.app.config.audit.AuditEventConverter;
import br.gov.prodepa.app.security.jwt.TokenProvider;

/**
 * @author thiago.soares (thiago.soares.jr@gmail.com)
 */
public class ResourcesSpring {

	
	@Produces
	public Environment getEnvironment() {
		Environment env = ApplicationContextHolder.getInstance().getBean(Environment.class);
		return env;
	}
	
	@Spring
	@Produces
	public JHipsterProperties getJHipsterProperties() {
		return ApplicationContextHolder.getInstance().getBean(JHipsterProperties.class);
	}
	
	@Produces
	public AuthenticationManager getAuthenticationManager() {
		return ApplicationContextHolder.getInstance().getBean(AuthenticationManager.class);
	}
	
	@Produces
	public AuditEventConverter getAuditEventConverter() {
		return ApplicationContextHolder.getInstance().getBean(AuditEventConverter.class);
	}
	
	@Spring
	@Produces
	public TokenProvider getTokenProvider() {
		return ApplicationContextHolder.getInstance().getBean(TokenProvider.class);
	}
	
}
