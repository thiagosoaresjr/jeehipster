package br.gov.prodepa.app.security.jwt;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import io.jsonwebtoken.ExpiredJwtException;

/**
 * Filters incoming requests and installs a Spring Security principal if a header corresponding to a valid user is
 * found.
 */
public class JWTFilter extends GenericFilterBean {

    private final Logger log = LoggerFactory.getLogger(JWTFilter.class);

    private TokenProvider tokenProvider;

    public JWTFilter(TokenProvider tokenProvider) {
        this.tokenProvider = tokenProvider;
    }
    
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
    	
    	try {
    		HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
    		
    		System.out.println("JWTFilter >> " + httpServletRequest.getRequestURL() + " // " + httpServletRequest.getQueryString());
    		
    		String jwt = resolveToken(httpServletRequest);

    		if (StringUtils.hasText(jwt)) {
    			
    			if (this.tokenProvider.validateToken(jwt)) {
    				Authentication authentication = this.tokenProvider.getAuthentication(jwt);
    				SecurityContextHolder.getContext().setAuthentication(authentication);
    			}
    			
    		} /*
    		//Processar o login do GD caso o Token não esteja presente.
    		else {
    			
    			System.out.println("JWTFilter >> LOGIN MODE  " + tokenProvider.getjHipsterProperties().getSecurity().getLoginMode());
    			
    			
    			if(httpServletRequest.getPathInfo() != null && !httpServletRequest.getPathInfo().equals("/authenticate")) {
    				
    				System.out.println(">");
    				
    				if(tokenProvider.getjHipsterProperties().getSecurity().getLoginMode() != null 
    						&& tokenProvider.getjHipsterProperties().getSecurity().getLoginMode().equals(tokenProvider.getjHipsterProperties().getSecurity().GD_LOGIN_MODE)) {
    					
    					System.out.println(">>");
    					
    					CloseableHttpClient httpclient = HttpClients.createDefault();
    					HttpGet httpGet = new HttpGet("http://localhost:8080/simasjee/api/authenticate");
    					httpGet.addHeader("Accept", "application/json");
    					CloseableHttpResponse response1 = httpclient.execute(httpGet);
    					
    					try {
    						
    						System.out.println("JWTFilter >> LoginProcede" + response1.getStatusLine());
    						
    						if(response1.getStatusLine().getStatusCode() == 200) {
    							
    							System.out.println(">>>");
    							
    							HttpEntity entity1 = response1.getEntity();
    							
    							String json = EntityUtils.toString(entity1);
    							
    							try {
    								JSONObject jsonObj = new JSONObject(json);
    								
    								jwt = (String) jsonObj.get("id_token");
    								
    								if (StringUtils.hasText(jwt)) {
    									
    									System.out.println("JWTFilter >> Login OK "  + jwt);
    									
    									if (this.tokenProvider.validateToken(jwt)) {
    										
    										System.out.println(">>>>");
    										
    										Authentication authentication = this.tokenProvider.getAuthentication(jwt);
    										SecurityContextHolder.getContext().setAuthentication(authentication);
    									}
    								}
    								
    							} catch (JSONException e) {
    								e.printStackTrace();
    							}
    						}
    					} finally {
    						response1.close();
    					}
    				}
    			}
    		}*/
    		
    		System.out.println(">>><<<");
            filterChain.doFilter(servletRequest, servletResponse);
        } catch (ExpiredJwtException eje) {
            log.info("Security exception for user {} - {}", eje.getClaims().getSubject(), eje.getMessage());
            ((HttpServletResponse) servletResponse).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
    
    
    
    private String resolveToken(HttpServletRequest request){
        String bearerToken = request.getHeader(JWTConfigurer.AUTHORIZATION_HEADER);
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            String jwt = bearerToken.substring(7, bearerToken.length());
            return jwt;
        }
        return null;
    }
}