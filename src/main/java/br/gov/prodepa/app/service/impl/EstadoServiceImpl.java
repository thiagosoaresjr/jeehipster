package br.gov.prodepa.app.service.impl;

import br.gov.prodepa.app.service.EstadoService;
import br.gov.prodepa.app.domain.Estado;
import br.gov.prodepa.app.repository.EstadoRepository;
import br.gov.prodepa.app.service.dto.EstadoDTO;
import br.gov.prodepa.app.service.mapper.EstadoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;


import org.springframework.stereotype.Service;

import javax.ejb.Stateless;
import javax.inject.Inject;

//import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Estado.
 */
//@Service
//@Transactional
@Stateless
public class EstadoServiceImpl implements EstadoService {

    private final Logger log = LoggerFactory.getLogger(EstadoServiceImpl.class);
    
    @Inject
    private EstadoRepository estadoRepository;
    
    @Inject
    private EstadoMapper estadoMapper;

    /**
     * Save a estado.
     *
     * @param estadoDTO the entity to save
     * @return the persisted entity
     */
    public EstadoDTO save(EstadoDTO estadoDTO) {
        log.debug("Request to save Estado : {}", estadoDTO);
        Estado estado = estadoMapper.estadoDTOToEstado(estadoDTO);
        estado = estadoRepository.save(estado);
        EstadoDTO result = estadoMapper.estadoToEstadoDTO(estado);
        return result;
    }

    /**
     *  Get all the estados.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<EstadoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Estados");
        
        System.out.println(pageable);
        
        Page<Estado> result = estadoRepository.findAll(pageable);
        return result.map(estado -> estadoMapper.estadoToEstadoDTO(estado));
    }

    /**
     *  Get one estado by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public EstadoDTO findOne(Long id) {
        log.debug("Request to get Estado : {}", id);
        Estado estado = estadoRepository.findOne(id);
        EstadoDTO estadoDTO = estadoMapper.estadoToEstadoDTO(estado);
        return estadoDTO;
    }

    
    /**
     *  Delete the  estado by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Estado : {}", id);
        estadoRepository.delete(id);
    }
}
