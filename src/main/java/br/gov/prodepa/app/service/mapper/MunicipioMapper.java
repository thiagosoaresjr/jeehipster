package br.gov.prodepa.app.service.mapper;

import br.gov.prodepa.app.domain.*;
import br.gov.prodepa.app.service.dto.MunicipioDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Municipio and its DTO MunicipioDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MunicipioMapper {

    @Mapping(source = "estado.id", target = "estadoId")
    MunicipioDTO municipioToMunicipioDTO(Municipio municipio);

    List<MunicipioDTO> municipiosToMunicipioDTOs(List<Municipio> municipios);

    @Mapping(source = "estadoId", target = "estado")
    Municipio municipioDTOToMunicipio(MunicipioDTO municipioDTO);

    List<Municipio> municipioDTOsToMunicipios(List<MunicipioDTO> municipioDTOs);

    default Estado estadoFromId(Long id) {
        if (id == null) {
            return null;
        }
        Estado estado = new Estado();
        estado.setId(id);
        return estado;
    }
}
