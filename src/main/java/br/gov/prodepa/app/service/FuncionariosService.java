package br.gov.prodepa.app.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.gov.prodepa.app.service.dto.FuncionariosDTO;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Funcionarios.
 */
public interface FuncionariosService {

    /**
     * Save a funcionarios.
     *
     * @param funcionariosDTO the entity to save
     * @return the persisted entity
     */
    FuncionariosDTO save(FuncionariosDTO funcionariosDTO);

    /**
     *  Get all the funcionarios.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<FuncionariosDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" funcionarios.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    FuncionariosDTO findOne(Long id);

    /**
     *  Delete the "id" funcionarios.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the funcionarios corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<FuncionariosDTO> search(String query, Pageable pageable);
}
