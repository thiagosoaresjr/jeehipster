package br.gov.prodepa.app.service.mapper;


import org.mapstruct.*;

import br.gov.prodepa.app.domain.Funcionarios;
import br.gov.prodepa.app.service.dto.FuncionariosDTO;

import java.util.List;

/**
 * Mapper for the entity Funcionarios and its DTO FuncionariosDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FuncionariosMapper {

    FuncionariosDTO funcionariosToFuncionariosDTO(Funcionarios funcionarios);

    List<FuncionariosDTO> funcionariosToFuncionariosDTOs(List<Funcionarios> funcionarios);

    Funcionarios funcionariosDTOToFuncionarios(FuncionariosDTO funcionariosDTO);

    List<Funcionarios> funcionariosDTOsToFuncionarios(List<FuncionariosDTO> funcionariosDTOs);
}
