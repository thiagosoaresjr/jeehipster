package br.gov.prodepa.app.service.dto;

import javax.validation.constraints.*;

import br.gov.prodepa.app.domain.Model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the Municipio entity.
 */
public class MunicipioDTO implements Serializable, Model {

    private Long id;

    @NotNull
    private String nome;


    private Long estadoId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getEstadoId() {
        return estadoId;
    }

    public void setEstadoId(Long estadoId) {
        this.estadoId = estadoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MunicipioDTO municipioDTO = (MunicipioDTO) o;

        if ( ! Objects.equals(id, municipioDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "MunicipioDTO{" +
            "id=" + id +
            ", nome='" + nome + "'" +
            '}';
    }
}
