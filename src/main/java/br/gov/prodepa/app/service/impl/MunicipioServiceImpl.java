package br.gov.prodepa.app.service.impl;

import br.gov.prodepa.app.service.MunicipioService;
import br.gov.prodepa.app.domain.Municipio;
import br.gov.prodepa.app.repository.MunicipioRepository;
import br.gov.prodepa.app.service.dto.MunicipioDTO;
import br.gov.prodepa.app.service.mapper.MunicipioMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Municipio.
 */
//@Service/
//@Transactional
@Stateless
public class MunicipioServiceImpl implements MunicipioService{

    private final Logger log = LoggerFactory.getLogger(MunicipioServiceImpl.class);
    
    @Inject
    private MunicipioRepository municipioRepository;

    @Inject
    private MunicipioMapper municipioMapper;

    /**
     * Save a municipio.
     *
     * @param municipioDTO the entity to save
     * @return the persisted entity
     */
    public MunicipioDTO save(MunicipioDTO municipioDTO) {
        log.debug("Request to save Municipio : {}", municipioDTO);
        Municipio municipio = municipioMapper.municipioDTOToMunicipio(municipioDTO);
        municipio = municipioRepository.save(municipio);
        MunicipioDTO result = municipioMapper.municipioToMunicipioDTO(municipio);
        return result;
    }

    /**
     *  Get all the municipios.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<MunicipioDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Municipios");
        Page<Municipio> result = municipioRepository.findAll(pageable);
        return result.map(municipio -> municipioMapper.municipioToMunicipioDTO(municipio));
    }

    /**
     *  Get one municipio by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public MunicipioDTO findOne(Long id) {
        log.debug("Request to get Municipio : {}", id);
        Municipio municipio = municipioRepository.findOne(id);
        MunicipioDTO municipioDTO = municipioMapper.municipioToMunicipioDTO(municipio);
        return municipioDTO;
    }

    /**
     *  Delete the  municipio by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Municipio : {}", id);
        municipioRepository.delete(id);
    }
}
