package br.gov.prodepa.app.service.dto;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import br.gov.prodepa.app.domain.Model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;


/**
 * A DTO for the Funcionarios entity.
 */
public class FuncionariosDTO implements Serializable, Model {

    private Long id;

    @NotNull
    private String nome;

    @NotNull
    private Integer idade;

    private Date dataNascimento;
    
    //@JsonDeserialize(using = LocalDateDeserializer.class)  
    //@JsonSerialize(using = LocalDateSerializer.class)
    //@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "UTC")
    //@JsonFormat(pattern = "yyyy-MM-dd", timezone = "UTC")
    //private LocalDate dataNascimento;

    private Long minSalary;

    private Long maxSalary;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    public Integer getIdade() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = idade;
    }
    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
    public Long getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(Long minSalary) {
        this.minSalary = minSalary;
    }
    public Long getMaxSalary() {
        return maxSalary;
    }

    public void setMaxSalary(Long maxSalary) {
        this.maxSalary = maxSalary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FuncionariosDTO funcionariosDTO = (FuncionariosDTO) o;

        if ( ! Objects.equals(id, funcionariosDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "FuncionariosDTO{" +
            "id=" + id +
            ", nome='" + nome + "'" +
            ", idade='" + idade + "'" +
            ", dataNascimento='" + dataNascimento + "'" +
            ", minSalary='" + minSalary + "'" +
            ", maxSalary='" + maxSalary + "'" +
            '}';
    }
}
