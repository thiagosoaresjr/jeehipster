package br.gov.prodepa.app.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import br.gov.prodepa.app.domain.Model;
import br.gov.prodepa.app.domain.enumeration.TipoEstado;

/**
 * A DTO for the Estado entity.
 */
public class EstadoDTO implements Serializable, Model {

    private Long id;

    @NotNull
    private String nome;

    @NotNull
    private String sigla;
    
    private TipoEstado tipo;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }
    public TipoEstado getTipo() {
        return tipo;
    }

    public void setTipo(TipoEstado tipo) {
        this.tipo = tipo;
    }

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EstadoDTO estadoDTO = (EstadoDTO) o;

        if ( ! Objects.equals(id, estadoDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "EstadoDTO{" +
            "id=" + id +
            ", nome='" + nome + "'" +
            ", sigla='" + sigla + "'" +
            ", tipo='" + tipo + "'" +
            '}';
    }
}
