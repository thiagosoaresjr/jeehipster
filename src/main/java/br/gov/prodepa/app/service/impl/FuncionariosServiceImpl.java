package br.gov.prodepa.app.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import br.gov.prodepa.app.domain.Funcionarios;
import br.gov.prodepa.app.repository.FuncionariosRepository;
import br.gov.prodepa.app.repository.search.FuncionariosSearchRepository;
import br.gov.prodepa.app.service.FuncionariosService;
import br.gov.prodepa.app.service.dto.FuncionariosDTO;
import br.gov.prodepa.app.service.mapper.FuncionariosMapper;

import org.springframework.stereotype.Service;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

//import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Funcionarios.
 */
//@Service
//@Transactional
@Stateless
public class FuncionariosServiceImpl implements FuncionariosService {

    private final Logger log = LoggerFactory.getLogger(FuncionariosServiceImpl.class);
    
    @Inject
    private FuncionariosRepository funcionariosRepository;

    @Inject
    private FuncionariosMapper funcionariosMapper;

    @Inject
    private FuncionariosSearchRepository funcionariosSearchRepository;

    /**
     * Save a funcionarios.
     *
     * @param funcionariosDTO the entity to save
     * @return the persisted entity
     */
    public FuncionariosDTO save(FuncionariosDTO funcionariosDTO) {
        log.debug("Request to save Funcionarios : {}", funcionariosDTO);
        Funcionarios funcionarios = funcionariosMapper.funcionariosDTOToFuncionarios(funcionariosDTO);
        funcionarios = funcionariosRepository.save(funcionarios);
        FuncionariosDTO result = funcionariosMapper.funcionariosToFuncionariosDTO(funcionarios);
        funcionariosSearchRepository.save(funcionarios);
        return result;
    }

    /**
     *  Get all the funcionarios.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<FuncionariosDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Funcionarios");
        Page<Funcionarios> result = funcionariosRepository.findAll(pageable);
        return result.map(funcionarios -> funcionariosMapper.funcionariosToFuncionariosDTO(funcionarios));
    }

    /**
     *  Get one funcionarios by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public FuncionariosDTO findOne(Long id) {
        log.debug("Request to get Funcionarios : {}", id);
        Funcionarios funcionarios = funcionariosRepository.findOne(id);
        FuncionariosDTO funcionariosDTO = funcionariosMapper.funcionariosToFuncionariosDTO(funcionarios);
        return funcionariosDTO;
    }

    /**
     *  Delete the  funcionarios by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Funcionarios : {}", id);
        funcionariosRepository.delete(id);
        //funcionariosSearchRepository.delete(id);
    }

    /**
     * Search for the funcionarios corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<FuncionariosDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Funcionarios for query {}", query);
        
        Page<Funcionarios> result = funcionariosSearchRepository.findByNomeContainingAllIgnoreCaseOrderByNomeAsc(query, pageable);
        
        //Page<Funcionarios> result = funcionariosSearchRepository.searchByNomeLikeAllIgnoreCaseOrderByNomeAsc("%"+query+"%", pageable);
        
        return result.map(funcionarios -> funcionariosMapper.funcionariosToFuncionariosDTO(funcionarios));
    }
}
