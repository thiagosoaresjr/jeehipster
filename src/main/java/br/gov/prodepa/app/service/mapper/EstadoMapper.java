package br.gov.prodepa.app.service.mapper;

import br.gov.prodepa.app.domain.*;
import br.gov.prodepa.app.service.dto.EstadoDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Estado and its DTO EstadoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EstadoMapper {

    EstadoDTO estadoToEstadoDTO(Estado estado);

    List<EstadoDTO> estadosToEstadoDTOs(List<Estado> estados);

    Estado estadoDTOToEstado(EstadoDTO estadoDTO);

    List<Estado> estadoDTOsToEstados(List<EstadoDTO> estadoDTOs);
}
