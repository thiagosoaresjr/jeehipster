package br.gov.prodepa.app.service;

import br.gov.prodepa.app.service.dto.EstadoDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Estado.
 */
public interface EstadoService {

    /**
     * Save a estado.
     *
     * @param estadoDTO the entity to save
     * @return the persisted entity
     */
    EstadoDTO save(EstadoDTO estadoDTO);

    /**
     *  Get all the estados.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<EstadoDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" estado.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    EstadoDTO findOne(Long id);

    /**
     *  Delete the "id" estado.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
