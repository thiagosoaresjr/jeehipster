package br.gov.prodepa.app.domain.enumeration;

/**
 * The TipoEstado enumeration.
 */
public enum TipoEstado {
    DF,NORMAL
}
