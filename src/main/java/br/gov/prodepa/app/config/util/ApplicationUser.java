package br.gov.prodepa.app.config.util;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class ApplicationUser extends User {

    private final String login;

    public ApplicationUser(String username, String password, boolean enabled,
        boolean accountNonExpired, boolean credentialsNonExpired,
        boolean accountNonLocked,
        Collection<GrantedAuthority> authorities, 
        String login) {

            super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);

            this.login = login;

    }

	public String getLogin() {
		return login;
	}

	@Override
	public String toString() {
		return "ApplicationUser [login=" + login + "]";
	}
    
    
}
