package br.gov.prodepa.app.config.util;


import java.util.Arrays;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

/**
 * 
 * Provedor de autenticação customizado. Deve consultar o ControleAcesso para processar o login.
 * 
 * @author thiago
 *
 */
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	private static Logger logger = LoggerFactory.getLogger(CustomAuthenticationProvider.class);

	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		String userName = authentication.getName().trim();

		String password = authentication.getCredentials().toString().trim();

		Authentication auth = null;

		//CustomLogin login = new CustomLogin();

		// Authenticate the user based on your custom logic

		//String role = login.getApplicationRole(userName, password, "ADMIN", "DEVELOPER");

		String role = "ADMIN";
		
		if (role != null) {

			Collection<GrantedAuthority> grantedAuths = Arrays.asList(new SimpleGrantedAuthority(role.trim()));

			ApplicationUser appUser = new ApplicationUser(userName, password, true, true, true, true, grantedAuths,	"TestEmail");

			auth = new UsernamePasswordAuthenticationToken(appUser, password, grantedAuths);

			return auth;

		} else {
			return null;
		}

	}

	public boolean supports(Class<? extends Object> authentication) {

		return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));

	}
}
