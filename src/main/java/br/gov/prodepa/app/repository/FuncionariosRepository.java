package br.gov.prodepa.app.repository;


import org.springframework.data.jpa.repository.*;

import br.gov.prodepa.app.domain.Funcionarios;

import java.util.List;

/**
 * Spring Data JPA repository for the Funcionarios entity.
 */
@SuppressWarnings("unused")
public interface FuncionariosRepository extends JpaRepository<Funcionarios,Long> {

}
