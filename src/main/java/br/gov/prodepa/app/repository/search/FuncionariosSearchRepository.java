package br.gov.prodepa.app.repository.search;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.gov.prodepa.app.domain.Funcionarios;
import br.gov.prodepa.app.domain.User;

//import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Funcionarios entity.
 */
public interface FuncionariosSearchRepository extends JpaRepository<Funcionarios,Long> /*extends ElasticsearchRepository<Funcionarios, Long>*/ {
	
	@Query(value = "select distinct f from Funcionarios f WHERE f.nome LIKE %:nome%",
	        countQuery = "select count(f) from Funcionarios f WHERE f.nome LIKE %:nome%")
	Page<Funcionarios> searchByNomeLikeAllIgnoreCaseOrderByNomeAsc(@Param("nome") String nome, Pageable pageable);
	
	
	Page<Funcionarios> findByNomeContainingAllIgnoreCaseOrderByNomeAsc(String nome, Pageable pageable);
	
}
