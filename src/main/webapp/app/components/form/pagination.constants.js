(function() {
    'use strict';

    angular
        .module('jHipsterProdepaApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
