(function() {
    'use strict';

    angular
        .module('jHipsterProdepaApp')
        .controller('MunicipioDetailController', MunicipioDetailController);

    MunicipioDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Municipio', 'Estado'];

    function MunicipioDetailController($scope, $rootScope, $stateParams, previousState, entity, Municipio, Estado) {
        var vm = this;

        vm.municipio = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('jHipsterProdepaApp:municipioUpdate', function(event, result) {
            vm.municipio = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
