(function() {
    'use strict';

    angular
        .module('jHipsterProdepaApp')
        .factory('FuncionariosSearch', FuncionariosSearch);

    FuncionariosSearch.$inject = ['$resource'];

    function FuncionariosSearch($resource) {
        var resourceUrl =  'api/funcionarios/search/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
