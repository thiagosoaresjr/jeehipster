(function() {
    'use strict';

    angular
        .module('jHipsterProdepaApp')
        .controller('FuncionariosDetailController', FuncionariosDetailController);

    FuncionariosDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Funcionarios'];

    function FuncionariosDetailController($scope, $rootScope, $stateParams, previousState, entity, Funcionarios) {
        var vm = this;

        vm.funcionarios = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('jhipsterApp:funcionariosUpdate', function(event, result) {
            vm.funcionarios = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
