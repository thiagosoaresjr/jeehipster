(function() {
    'use strict';

    angular
        .module('jHipsterProdepaApp')
        .controller('FuncionariosDeleteController',FuncionariosDeleteController);

    FuncionariosDeleteController.$inject = ['$uibModalInstance', 'entity', 'Funcionarios'];

    function FuncionariosDeleteController($uibModalInstance, entity, Funcionarios) {
        var vm = this;

        vm.funcionarios = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Funcionarios.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
