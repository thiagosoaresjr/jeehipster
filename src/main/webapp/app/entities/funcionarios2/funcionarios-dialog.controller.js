(function() {
    'use strict';

    angular
        .module('jHipsterProdepaApp')
        .controller('FuncionariosDialogController', FuncionariosDialogController);

    FuncionariosDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Funcionarios'];

    function FuncionariosDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Funcionarios) {
        var vm = this;

        vm.funcionarios = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.funcionarios.id !== null) {
                Funcionarios.update(vm.funcionarios, onSaveSuccess, onSaveError);
            } else {
                Funcionarios.save(vm.funcionarios, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('jHipsterProdepaApp:funcionariosUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dataNascimento = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
