(function() {
    'use strict';
    angular
        .module('jHipsterProdepaApp')
        .factory('Funcionarios', Funcionarios);

    Funcionarios.$inject = ['$resource', 'DateUtils'];

    /*function Funcionarios ($resource, DateUtils) {
        var resourceUrl =  'api/funcionarios/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dataNascimento = DateUtils.convertDateTimeFromServer(data.dataNascimento);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }*/
    
    function Funcionarios ($resource, DateUtils) {
        var resourceUrl =  'api/funcionarios/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dataNascimento = DateUtils.convertLocalDateFromServer(data.dataNascimento);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.dataNascimento = DateUtils.convertLocalDateToServer(copy.dataNascimento);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.dataNascimento = DateUtils.convertLocalDateToServer(copy.dataNascimento);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
