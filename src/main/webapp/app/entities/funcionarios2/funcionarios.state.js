(function() {
    'use strict';

    angular
        .module('jHipsterProdepaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('funcionarios', {
            parent: 'entity',
            url: '/funcionarios?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Funcionarios'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/funcionarios/funcionarios.html',
                    controller: 'FuncionariosController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
            }
        })
        .state('funcionarios-detail', {
            parent: 'entity',
            url: '/funcionarios/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Funcionarios'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/funcionarios/funcionarios-detail.html',
                    controller: 'FuncionariosDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Funcionarios', function($stateParams, Funcionarios) {
                    return Funcionarios.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'funcionarios',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('funcionarios-detail.edit', {
            parent: 'funcionarios-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/funcionarios/funcionarios-dialog.html',
                    controller: 'FuncionariosDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Funcionarios', function(Funcionarios) {
                            return Funcionarios.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('funcionarios.new', {
            parent: 'funcionarios',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/funcionarios/funcionarios-dialog.html',
                    controller: 'FuncionariosDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                nome: null,
                                idade: null,
                                dataNascimento: null,
                                minSalary: null,
                                maxSalary: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('funcionarios', null, { reload: 'funcionarios' });
                }, function() {
                    $state.go('funcionarios');
                });
            }]
        })
        .state('funcionarios.edit', {
            parent: 'funcionarios',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/funcionarios/funcionarios-dialog.html',
                    controller: 'FuncionariosDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Funcionarios', function(Funcionarios) {
                            return Funcionarios.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('funcionarios', null, { reload: 'funcionarios' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('funcionarios.delete', {
            parent: 'funcionarios',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/funcionarios/funcionarios-delete-dialog.html',
                    controller: 'FuncionariosDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Funcionarios', function(Funcionarios) {
                            return Funcionarios.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('funcionarios', null, { reload: 'funcionarios' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
