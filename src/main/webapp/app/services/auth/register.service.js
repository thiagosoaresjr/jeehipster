(function () {
    'use strict';

    angular
        .module('jHipsterProdepaApp')
        .factory('Register', Register);

    Register.$inject = ['$resource'];

    function Register ($resource) {
        return $resource('api/register', {}, {});
    }
})();
